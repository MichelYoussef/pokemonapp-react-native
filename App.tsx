import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { MainNavigator } from './src/navigators/MainNavigator';
import { NavigationContainer } from '@react-navigation/native';
import { NativeBaseProvider } from 'native-base'
import { useEffect } from 'react';
import { LogBox } from 'react-native';

const queryClient = new QueryClient()

export default function App() {

  //This use effect ignores a Log that doesnt affect the application but the native-base library uses SSRProvider which is not supported in React 18
  useEffect(() => {
    LogBox.ignoreLogs(['In React 18, SSRProvider is not necessary and is a noop. You can remove it from your app.']);
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <NativeBaseProvider>
        <NavigationContainer>
          <MainNavigator />
        </NavigationContainer>
      </NativeBaseProvider>
    </QueryClientProvider>
  );
}
