# Application Pokémon

Cette application React Native récupère des données de l'API Pokémon et affiche des informations sur les Pokémon.

## Exigences
1. **React Native & TypeScript**: L'application est développée avec React Native et TypeScript.

2. **API**:  Les données des Pokémon sont extraites de l'[API Pokémon](https://pokeapi.co).

3. **Libraries**: La gestion de l'état est gérée avec @tanstack/reactquery. La mise en page est effectuée en utilisant la bibliothèque Native Base.

## Installation
1. Clonez le dépôt:
    ```bash
    git clone https://gitlab.com/MichelYoussef/pokemonapp-react-native.git

2. Accédez au répertoire du projet:
    ```bash
    cd <répertoire-du-projet>

3. Installez les dépendances :
    ```bash
    // en utilisant npm
    npm install
    // en utilisant yarn
    yarn install

## Lancement de l'application

1. Démarrez le serveur de développement:

    Vous devez avoir Expo CLI installé globalement: 
    ```bash
    // en utilisant npm
    npm install --global expo-cli 

    // en utilisant yarn
    yarn add global expo-cli

    // en utilisant npm
    npm start  
    
    // en utilisant yarn
    yarn start
  

2. Pour exécuter l'application avec Expo Go :

    <strong>2.1</strong> Installez Expo Go sur votre appareil mobile et ouvrez l'application.

    <strong>2.2</strong> Scannez le code QR affiché dans votre terminal après avoir lancé le serveur de développement.

    <strong>2.3</strong> Attendez que l'application se construise et se charge.

3. Pour exécuter l'application sur un émulateur Android :

    <strong>3.1</strong> Branchez votre appareil Android ou ouvrez un émulateur.

    <strong>3.2</strong> Appuyez sur 'a' dans le terminal après avoir lancé npm start ou yarn start.    

4. Pour exécuter l'application sur un émulateur iOS :

    <strong>4.1</strong> Branchez votre appareil iOS.

    <strong>4.2</strong> Appuyez sur 'i' dans le terminal après avoir lancé npm start ou yarn start.


# Documentation

### Approche
Cette application React Native a été développée avec une approche centrée sur l'utilisateur et modulaire. Elle vise à offrir une expérience fluide aux utilisateurs pour découvrir des informations sur les Pokémon. J'ai organisé la base de code et les composants pour qu'ils soient propres, faciles à entretenir et facilement extensibles.

### Choix techniques
- **Développement multiplateforme**: L'application exploite React Native, un puissant framework pour créer des applications mobiles pour Android et iOS à l'aide d'un seul code source. TypeScript a été utilisé pour assurer la sécurité des types et une meilleure qualité du code.

- **Gestion de l'état**: Au lieu de Redux ou RTK, j'ai opté pour '@tanstack/reactquery' pour gérer l'état de l'application et effectuer des requêtes API, car :

     - **Récupération de données simplifiée**: React Query simplifie le processus de récupération et de gestion des données à partir des API. Il abstrait bon nombre des complexités associées aux bibliothèques traditionnelles de gestion de l'état. Cette simplicité conduit à une récupération de données plus fluide et efficace.

    - **Données synchronisées avec le serveur**: React Query offre une prise en charge intégrée des données synchronisées avec le serveur, ce qui facilite le maintien de la cohérence des données du client avec l'état du serveur. Cela est essentiel pour garantir que notre application affiche toujours des informations à jour sur les Pokémon.

    - **Mise en cache et invalidation de requête**: React Query offre une mise en cache automatique et une invalidation de requête. Cela signifie que les données sont mises en cache de manière efficace et mises à jour lorsque nécessaire, réduisant les requêtes API inutiles et améliorant les performances de l'application.

    - **Gestion des erreurs**: React Query inclut des mécanismes de gestion des erreurs robustes, simplifiant le processus de gestion des erreurs réseau et améliorant l'expérience utilisateur globale. Cela garantit que notre application gère de manière élégante les problèmes imprévus qui peuvent survenir lors de la récupération des données.

- **Mise en page**: Native Base a été choisi pour la mise en page des composants, offrant une expérience de conception fluide et cohérente. Ce choix a été fait pour gagner du temps de développement et maintenir un aspect poli.

- **Expo**: Le projet est basé sur Expo, un ensemble d'outils et de services qui simplifient le processus de développement. Il nous permet de tester rapidement l'application sur des appareils physiques avec Expo Go.

### Détails supplémentaires
- L'application comprend deux écrans principaux : la liste des Pokémon (Home.tsx) et les détails des Pokémons (Details.tsx). Cela permet aux utilisateurs de voir facilement une liste de Pokémons et d'accéder aux informations détaillées sur chacun d'entre eux.

- L'utilisation de '@tanstack/reactquery' et de Native Base contribue à offrir une expérience utilisateur réactive et visuellement agréable.

## Fonctionnalités supplémentaires
- **Chargement infini**: En plus des exigences de base, j'ai mis en œuvre la fonctionnalité bonus de chargement infini. En naviguant dans la liste des Pokémon, les utilisateurs bénéficieront d'un chargement continu et fluide de Pokémon supplémentaires, créant une expérience de navigation plus engageante et immersive.

- **Fonction de recherche**: J'ai incorporé une puissante fonction de recherche qui permet aux utilisateurs de trouver rapidement leur Pokémon préféré. Les utilisateurs peuvent simplement saisir le nom ou l'ID (numéro d'ordre), et l'application affichera instantanément la page de détails de ce Pokémon particulier.

Ces fonctionnalités supplémentaires visent à améliorer l'expérience utilisateur en offrant un moyen fluide et agréable d'explorer et de découvrir les Pokémon dans l'application.

J'ai conçu cette application avec une interface conviviale, une base de code évolutive et une gestion efficace de l'état pour répondre efficacement aux exigences.