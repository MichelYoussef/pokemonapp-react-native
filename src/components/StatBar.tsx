import React from 'react';
import { Box, Text, Progress } from 'native-base';

// Define props for the StatBar component.
interface StatBarProps {
  statName: string;
  statValue: number;
  color: string;
}

// StatBar component to display a stat with a progress bar.
const StatBar: React.FC<StatBarProps> = ({ statName, statValue, color }) => {
  return (
    <Box width="100%">
      <Text fontSize="md" textTransform="capitalize">{statName}</Text>
      <Progress
        size="md"
        colorScheme={color}
        value={statValue}
        borderRadius="xl"
        width="90%"
      />
      <Text>{statValue}</Text>
    </Box>
  );
};

export default StatBar;