import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { Home } from '../screens/Home'
import { Detail } from '../screens/Detail';
import { Search } from '../screens/Search';
import { TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons'
import { MainStackParamList } from './types';

// Create a stack navigator for the main app navigation using MainStackParamList.
const Stack = createNativeStackNavigator<MainStackParamList>();

export function MainNavigator() {
    return (
        <Stack.Navigator>
            {/* Home screen with navigation options */}
            <Stack.Screen
                name='Home'
                component={Home} 
                options={({ navigation }) => ({
                    headerLargeTitle: true,
                    headerTitle: "Pokémon",
                    headerRight: () => (
                        <TouchableOpacity onPress={() => navigation.navigate('Search')}>
                            <MaterialIcons name="search" color="black" size={32} />
                        </TouchableOpacity>
                    )
                })}
            />
            {/* Detail screen with navigation options */}
            <Stack.Screen 
                name='Detail' 
                component={Detail} 
                options={{
                headerTitle: '',
                headerTransparent: true,
                headerTintColor: 'white',
            }} 
            />
            {/* Created a modal stack group for the Search screen. */}
            <Stack.Group screenOptions={{ presentation: 'modal' }}>
                <Stack.Screen name='Search' component={Search} />
            </Stack.Group>
        </Stack.Navigator>
    )
}