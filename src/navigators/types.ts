import type{ NativeStackScreenProps } from '@react-navigation/native-stack'

// Definition of the possible screens and their associated parameters in the main stack.
export type MainStackParamList = {
    Home: undefined;
    Search: undefined;
    Detail: { name: string };
}

// Creation of a type that represents the screen props for the main stack.
// It allows us to specify which screen's props we want to access.
export type MainStackScreenProps<T extends keyof MainStackParamList> = NativeStackScreenProps<MainStackParamList, T>;
