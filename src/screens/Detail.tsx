import { MainStackScreenProps } from "../navigators/types"
import { Pokemon, PokemonStats, Species, fetchFn, fetchPokemon, fetchPokemonStats } from "../utils/api";
import { useQuery } from "@tanstack/react-query";
import { AspectRatio, Image, Text, Heading, Stack, HStack, Center, Skeleton, ScrollView} from 'native-base'
import { formatNumber, getTypeColor, removeEscapeCharacters } from "../utils/helper";
import StatBar from "../components/StatBar";

export function Detail({ route }: MainStackScreenProps<'Detail'>) {
    
    const { name } = route.params

    // Fetch Pokémon data using useQuery
    const { data } = useQuery<Pokemon>({
        queryKey: ['pokemon', name],
        queryFn: async () => {
            const result = await fetchPokemon(name);
            return result;
        }
    });

    // Fetch Pokémon species data using useQuery
    const {isLoading: isSpeciesLoading, data: species } = useQuery<Species>({
        queryKey: ['species', name],
        queryFn: async() => {
            const result = await fetchFn(data?.species.url || '');
            return result;
        },
        enabled: !!data, // Fetch species data only when Pokémon data is available
    })

    // Fetch Pokémon stats using useQuery
    const { data: stats, isLoading: isStatsLoading } = useQuery<PokemonStats>({
        queryKey: ['stats', name],
        queryFn: async () => {
          const result = await fetchPokemonStats(name);
          return result;
        },
        enabled: !!data,
      });
    
    if (!data) return null;
    return(
        <ScrollView>
             <Stack>
                {/* Display Pokémon image and basic information */}
                <Center safeArea backgroundColor={getTypeColor(data.types[0].type.name ) + '.500'}>
                    <AspectRatio ratio={1} width="80%">
                        <Image source={{
                            uri: data.sprites.other['official-artwork'].front_default
                        }}
                        alt="image" />
                    </AspectRatio>
                    <HStack justifyContent="space-between" width="100%" alignItems="center" position="absolute" bottom={0} left={0} right={0}>
                        <Heading color="white" textTransform="capitalize" size="2xl">{name}</Heading>
                        <Heading color="white">#{formatNumber(data.id)}</Heading>
                    </HStack>
                </Center>
                <Stack p={3}>
                        <HStack justifyContent="center">
                            {data.types.map(type => (
                                <Center key={type.type.name} backgroundColor={getTypeColor(type.type.name) + '.500'} rounded="full" p={1} minW="32" _text={{color: 'white', fontSize: 'lg', fontWeight: 'bold', textTransform: 'capitalize'}} mx={2}>
                                    {type.type.name}
                                </Center>
                            ))}
                        </HStack>
                        <Center>
                        {isSpeciesLoading && <Skeleton.Text />} 
                        {!!species && (
                            <Text fontSize="xl" mt="4" fontStyle="italic">
                                {"“"}
                                {removeEscapeCharacters(species.flavor_text_entries[0].flavor_text)}
                                {"”"}
                            </Text>
                            )}
                        </Center>

                        {!!stats && (
                            <Stack>
                                {/* Display Pokémon stats */}
                                <Heading fontSize="xl" mt="5" mb="2" color={getTypeColor(data.types[0].type.name) + '.500'}>Stats</Heading>
                                <HStack justifyContent="space-between" flexDirection="column">

                                    {stats.stats.map((stat) => (
                                        <HStack
                                            key={stat.stat.name}
                                            justifyContent="space-between"
                                            width="100%"
                                            mb="2"
                                        >
                                        <StatBar statName={stat.stat.name} statValue={stat.base_stat} color={getTypeColor(data.types[0].type.name)} />
                                        </HStack>
                                    ))}

                                </HStack>
                            </Stack>
                        )}
                </Stack>
            </Stack>
        </ScrollView>
    )
}

