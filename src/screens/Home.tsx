import { PokemonCard } from '../components/PokemonCard';
import { useInfiniteQuery } from '@tanstack/react-query';
import { AllPokemon, fetchAllPokemon } from '../utils/api'
import { Center, Spinner, FlatList } from 'native-base'

export function Home() {

// Use the useInfiniteQuery hook to fetch and manage data for an infinite scroll list.
const {
  data,
  isLoading,
  hasNextPage,
  fetchNextPage,
  isFetchingNextPage
} = useInfiniteQuery<AllPokemon>({
  queryKey: ['pokemons'],
  queryFn: async({ pageParam}) => {
    const response = await fetchAllPokemon({ pageParam: pageParam as string});
    return response;
  },
  getNextPageParam: (lastPage) => lastPage.next,
  initialPageParam: undefined
});

// Load more Pokémon data if there is a next page and not currently fetching
const loadMore = () => {
    if(hasNextPage && !isFetchingNextPage) {
      fetchNextPage();
    }
  };

  // Display a loading spinner while fetching data
  if (isLoading) return (
    <Center flex={1}>
      <Spinner size="lg" color="black" />
    </Center>
  )

  if (!data) return null;

  // Display a list of Pokémon cards with infinite loading
  return (
      <FlatList 
        data={data.pages.flatMap((page) => page.results)}
        keyExtractor={(item) => item.name}
        renderItem={({item}) => <PokemonCard name={item.name} />}
        onEndReached={loadMore}
        numColumns={2}
        contentInsetAdjustmentBehavior='automatic'
        ListFooterComponent={() => isFetchingNextPage ? <Spinner mt="4" size="lg" color="black" /> : null}
        _contentContainerStyle={{ p: 2, bg: 'white'}}
      />
  );
}