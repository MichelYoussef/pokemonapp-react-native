import { useState, useEffect } from 'react'
import { Stack, Input, Spinner, Icon, Text, Center} from 'native-base'
import { MaterialIcons } from '@expo/vector-icons'
import { useQuery } from '@tanstack/react-query'
import { Pokemon, fetchPokemon } from '../utils/api'
import { MainStackScreenProps } from '../navigators/types'

export function Search({ navigation }: MainStackScreenProps<'Search'> ) {

    const [text, setText] = useState('')

    // UseQuery hook to fetch Pokémon data
    const { data, fetchStatus, error } = useQuery<Pokemon>({
        queryKey: ['pokemon', text],
        queryFn: async() => {
            const result = await fetchPokemon(text.toLowerCase());
            return result;
        },
        enabled: !!text, // Fetch data only when text is not empty
    })

    // Redirect to the 'Detail' screen when data is available
    useEffect(() => {
        if (data) {
            navigation.replace('Detail', { name: data.name })
        }
    }, [data])
    
    return(
        <Stack flex={1} p="4">
            {/* Search by name or id */}
            <Input 
                placeholder='Search Pokemon'
                backgroundColor="white"
                rounded="xl"
                py="3"
                px="1"
                fontSize="14"
                returnKeyType="search"
                onSubmitEditing={({ nativeEvent }) => setText(nativeEvent.text)}
                InputLeftElement={
                    <Icon 
                     as={MaterialIcons} 
                     name="search"
                     m="2" 
                     ml="3" 
                     size="6" 
                     color="gray.400" 
                     />
                }
            />

            <Center flex={1}>
                {/* Display an error message if the search has no results */}
                {!!error && (
                    <Text color="gray.500" fontSize="xl">
                        No results found for {text}
                    </Text>
                )}

                {/* Display a loading spinner while fetching data */}
                {fetchStatus === 'fetching' && <Spinner size="lg" />}
            </Center>
        </Stack>
    )
}