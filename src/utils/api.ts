// This interface represents the structure of a Pokémon, including its name, id, sprites, types, and species.
export interface Pokemon {
    name: string;
    id: number;
    sprites: {
        other: {
            'official-artwork': {
                front_default: string;
            }
        }
    }
    types: {
        slot: number;
        type: {
            name: string;
        };
    }[];
    species: {
        url: string;
    }
}

// This interface represents a list of Pokémon with count, next, previous, and an array of Pokémon results.
export interface AllPokemon {
    count: number;
    next: string;
    previous?: string;
    results: {
        name: string;
        url: string;
    }[];
}

// This interface represents the flavor text entries of a Pokémon species.
export interface Species {
    flavor_text_entries: {
        flavor_text: string;
    }[];
}

// This interface represents the stats and types of a Pokémon.
export interface PokemonStats {
    stats: {
      stat: {
        name: string;
      };
      base_stat: number;
    }[];
    types: {
        slot: number;
        type: {
            name: string;
        };
    }[];
  }

// This function fetches data from a specified API endpoint and parse it as JSON.
export async function fetchFn(endpoint: string) {
    const response = await fetch(endpoint)
    return response.json();
}

// This function fetches a list of Pokémon from the PokeAPI, with optional pagination support.
export async function fetchAllPokemon({ pageParam }: { pageParam?: string}) {
    const response = await fetch(pageParam || 'https://pokeapi.co/api/v2/pokemon/')
    return response.json();
}

// This function fetches details of a specific Pokémon by name.
export async function fetchPokemon(name: string) {
    const response = await fetch('https://pokeapi.co/api/v2/pokemon/' + name)
    if(!response.ok){
        throw new Error(`Pokemon ${name} not found`);
    }
    return response.json();
}

// This function fetches the stats of a specific Pokémon by name.
export async function fetchPokemonStats(name: string) {
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`);
    if (!response.ok) {
      throw new Error(`Pokemon stats for ${name} not found`);
    }
    return response.json();
  }